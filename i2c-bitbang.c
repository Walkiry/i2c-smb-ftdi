#include <stdio.h>
#include <stdlib.h>
#include <ftdi.h>
#include <sys/time.h>
#include <string.h>
#include <stdbool.h>

/* Define the port used for I2C data and
 * clk as shown above to access them pin wise 
 */
#define I2C_DATA i2c_sda_set
#define I2C_CLK i2c_scl_set 
#define RSHUNT (0.10)


enum level {
	LOW,
	HIGH
};

enum access {
	READ,
	WRITE
};


struct i2c_bitbang {
	struct ftdi_context *ftdi;	
	enum level sda_lvl;
	enum level scl_lvl; 
	enum access bus_acc;

	enum access acc;
	unsigned char addr;
	unsigned char reg;

	unsigned char data;
};

int i2c_write_read_to_ftdi(struct i2c_bitbang *i2c)
{
	int pins = 0b00010000;
	pins |= (i2c->bus_acc << 5) | (i2c->sda_lvl << 1) | i2c->scl_lvl;


	if (ftdi_set_bitmode(i2c->ftdi, pins, BITMODE_CBUS)) {
		ftdi_usb_close(i2c->ftdi);
		ftdi_deinit(i2c->ftdi);
		return -1;
	}

	return 0;
}

void i2c_scl_set(struct i2c_bitbang *i2c, enum level lvl)
{
	i2c->scl_lvl = lvl;
	i2c_write_read_to_ftdi(i2c);
}

unsigned char i2c_sda_set(struct i2c_bitbang *i2c, enum level lvl)
{
	int rc = 0;
	i2c->sda_lvl = lvl;
	i2c_write_read_to_ftdi(i2c);

	if (i2c->bus_acc == READ) {
		rc = ftdi_read_pins(i2c->ftdi, &i2c->data);			
		i2c->data = i2c->data & 0x02;
		i2c->data = i2c->data >> 1;
	}

	return rc;
}

int init_ftdi(struct i2c_bitbang *i2c)
{
	int ret;
	if ((i2c->ftdi = ftdi_new()) == 0) {
		fprintf(stderr, "ftdi_new failed\n");
		return EXIT_FAILURE;
	}

	if ((ret = ftdi_usb_open(i2c->ftdi, 0x0403, 0x6001)) < 0)
	{
		fprintf(stderr, "unable to open ftdi device: %d (%s)\n",
			       		ret, ftdi_get_error_string(i2c->ftdi));
 		ftdi_deinit(i2c->ftdi); 
		return EXIT_FAILURE;
	}

	return ret;
}

void print_usage(const char *app_name)
{
	fprintf(stdout, "Usage: %s -[rw] [slave_addr] [reg_addr] [value]\n", strrchr(app_name, '/'));
	fprintf(stdout, "\t\t -r: Read of the register [reg_addr] from I2C slave at address [slave_addr] \n");
	fprintf(stdout, "\t\t -w: Write the value [value] to the register [reg_addr] to I2C slave at address [slave_addr]\n");
	fprintf(stdout, "\t\t slave_addr: I2C slave address\n");
	fprintf(stdout, "\t\t reg_address: register to access within the slave\n");
}

int init_get_args(struct i2c_bitbang *i2c, int argc, char **argv)
{
	if (argc < 2 || argc > 4) {
		print_usage(argv[0]);
		return -1;
	}

	if (!strcmp("-w", argv[1])) {
		i2c->acc = WRITE;
	} else if (!strcmp("-r", argv[1])) {
		i2c->acc = READ;
	} else {
		print_usage(argv[0]);
		return -1;
	}

	i2c->addr = strtol(strrchr(argv[2], 'x') + 1, NULL, 16) << 1;
	i2c->reg = strtol(strrchr(argv[3], 'x') + 1, NULL, 16);

	return 0;
}

void I2C_WAIT_HALF_PERIOD(void)
{
	struct timeval tv_start, tv_end; 
	gettimeofday(&tv_start, NULL);
	gettimeofday(&tv_end, NULL);

	while ((tv_end.tv_usec - tv_start.tv_usec) < 475) {
		gettimeofday(&tv_end, NULL);
	}
}

/* I2C Start - bit bang */
void I2C_START(struct i2c_bitbang *i2c) {
	/* I2C Start condition, data line goes low when clock is high */
	enum level save_bus_acess = i2c->bus_acc;
	i2c->bus_acc = WRITE;

	I2C_DATA(i2c, HIGH);
	I2C_CLK(i2c, HIGH);
	I2C_WAIT_HALF_PERIOD();
	I2C_DATA(i2c, LOW);
	I2C_WAIT_HALF_PERIOD();

	i2c->bus_acc = save_bus_acess;
}

/* I2C Stop - bit bang */
void I2C_STOP(struct i2c_bitbang *i2c)
{
	enum level save_bus_acess = i2c->bus_acc;
	i2c->bus_acc = WRITE;

	/* I2C Stop codition, clock goes high when data is low */
	I2C_CLK(i2c, LOW);
	I2C_DATA(i2c, LOW);
	I2C_WAIT_HALF_PERIOD();

	I2C_CLK(i2c, HIGH);
	I2C_DATA(i2c, HIGH);
	I2C_WAIT_HALF_PERIOD();

	i2c->bus_acc = save_bus_acess;
}

int I2C_NACK_ACK_MASTER(struct i2c_bitbang *i2c, bool ack)
{
	int value = 0;
	enum level save_bus_acess = i2c->bus_acc;
	i2c->bus_acc = WRITE;
	/* Clock high, valid ACK */
	I2C_CLK(i2c, LOW);

	/* Chose acknowledge or not */
	if (ack) {
		I2C_DATA(i2c, LOW);
	} else {
		I2C_DATA(i2c, HIGH);
	}
	I2C_WAIT_HALF_PERIOD();

	I2C_CLK(i2c, HIGH);					
	I2C_WAIT_HALF_PERIOD();

	i2c->bus_acc = save_bus_acess;

	return value;
}

int I2C_ACK_SLAVE(struct i2c_bitbang *i2c)
{
	int value = 0;
	enum level save_bus_acess = i2c->bus_acc;

	/* Clock high, valid ACK */
	i2c->bus_acc = READ;
	I2C_CLK(i2c, LOW);
	I2C_DATA(i2c, LOW);
	I2C_WAIT_HALF_PERIOD();

	I2C_CLK(i2c, HIGH);					
	I2C_WAIT_HALF_PERIOD();
	value = I2C_DATA(i2c, LOW);

	i2c->bus_acc = save_bus_acess;

	return value;
}

/* I2C Write - bit bang */
void I2C_WRITE(struct i2c_bitbang *i2c, unsigned char data)
{
	unsigned char outBits;
	i2c->bus_acc = WRITE;
	/* 8 bits */
	for (outBits = 0; outBits < 8; outBits++) {
		I2C_CLK(i2c, LOW);
		if(data & 0x80)
			I2C_DATA(i2c, HIGH);
		else
			I2C_DATA(i2c, LOW);
		/* Generate clock for 8 data bits */
		I2C_WAIT_HALF_PERIOD();
		I2C_CLK(i2c, HIGH);
		I2C_WAIT_HALF_PERIOD();
		data  <<= 1;
	}

	I2C_ACK_SLAVE(i2c);
}

unsigned int I2C_READ(struct i2c_bitbang *i2c)
{
	char inBits;
	unsigned int data = 0;
	enum level save_bus_acess = i2c->bus_acc;
	i2c->bus_acc = READ;

	/* 8 bits */
	for (inBits = 7; inBits >= 0; inBits--) {
		I2C_CLK(i2c, LOW);
	 	I2C_DATA(i2c, LOW);
		data |= (i2c->data << inBits);
		I2C_WAIT_HALF_PERIOD();
		I2C_CLK(i2c, HIGH);
		I2C_WAIT_HALF_PERIOD();
	}

	i2c->bus_acc = save_bus_acess;
	return data;
}

/* Examble for writing to I2C Slave */
void writeI2CSlave (struct i2c_bitbang *i2c)	
{
	/* Start */
	I2C_START(i2c);
	/* Slave address */
	I2C_WRITE(i2c, i2c->addr);
	/* Slave control byte */
	I2C_WRITE(i2c, i2c->reg);
	/* Slave data */
	I2C_WRITE(i2c, i2c->data);
	/* Stop */
	I2C_STOP(i2c);
}

/* Examble for reading from I2C Slave */
int readI2CSlave(struct i2c_bitbang *i2c)
{
	unsigned char intdata[2];
	short sdata;

	/* Start */
	I2C_START(i2c); 
	/* Slave address */
	I2C_WRITE(i2c, i2c->addr);
	/* Slave control byte */
	I2C_WRITE(i2c, i2c->reg);
	/* Stop */
	I2C_STOP(i2c);
	/* Start */
	I2C_START(i2c);
	/* Slave address + read */
	I2C_WRITE(i2c, i2c->addr | 1);
	/* Read */
	intdata[0] = I2C_READ(i2c);
	I2C_NACK_ACK_MASTER(i2c, true);

	intdata[1] = I2C_READ(i2c);
	I2C_NACK_ACK_MASTER(i2c, false);
	I2C_STOP(i2c);

	sdata = (intdata[0] << 8) + intdata[1];
	fprintf(stdout, "Value retrieved 0x%02x%02x\n",intdata[0], intdata[1]);
	fprintf(stdout, "Value retrieved %f mv\n", sdata * 0.01f * (1 / RSHUNT));

	return 0;                 
}

int main(int argc, char **argv)
{
	struct i2c_bitbang i2c;
	unsigned char data;
	int rc = 0;
	memset(&i2c, 0, sizeof(i2c));

	if (init_get_args(&i2c, argc, argv)) {
		return -1;
	}


	if (init_ftdi(&i2c)) {
		return -1;
	}

	i2c.sda_lvl = HIGH;
	i2c.scl_lvl = HIGH;
	i2c_write_read_to_ftdi(&i2c);

	usleep(50000);
	switch (i2c.acc) {
		case READ:
			readI2CSlave(&i2c);
			break;
		case WRITE:
			writeI2CSlave(&i2c);
			printf("Data write from the i2c \n");
			break;
		default:
			break;
	}

	i2c.sda_lvl = HIGH;
	i2c.scl_lvl = HIGH;
	i2c_write_read_to_ftdi(&i2c);
	return rc;
}
